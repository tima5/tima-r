#!/usr/bin/env bash

if [ ! -f LICENSE ]; then
  echo "Sorry, you need to run that from the root of the project."
  exit 1
fi

